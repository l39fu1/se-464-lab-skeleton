const { request } = require("express");
const logger = require("./logger.js");

const db = process.argv[3];

const {
  init,
  queryRandomProduct,
  queryUserById,
  queryAllProducts,
  queryProductById,
  queryAllCategories,
  queryAllOrders,
  queryOrdersByUser,
  queryOrderById,
  queryAllUsers,
  insertOrder,
  updateUser,
} = require(db === "dynamo"
  ? "./dynamo_db.js"
  : db === "sql"
  ? "./mysql_db.js"
  : "");

let requestCount = 0;

const logRequest = (requestType,requestPath) => {
  requestCount++;
  logger.info(
    `Request: ${requestType} ${requestPath} received at ${new Date().toISOString()}`
  );
  logger.info(`Total number of requests: ${requestCount}`);

}

const getRandomProduct = async (req, res) => {
  logRequest("GET", "/randomproduct");
  const randProd = await queryRandomProduct();
  console.log(randProd)
  res.send(randProd);
};

const getProduct = async (req, res) => {
  const { productId } = req.params;
  logRequest("GET", "/product/"+productId)
  const product = await queryProductById(productId);
  res.send(product);
};

const getProducts = async (req, res) => {
  logRequest("GET","/products");
  const { category } = req.query;
  const products = await queryAllProducts(category);
  res.send(products);
};

const getCategories = async (req, res) => {
  logRequest("GET", "/categories");
  const categories = await queryAllCategories();
  res.send(categories);
};

const getAllOrders = async (req, res) => {
  logRequest("GET","/allorders");
  const orders = await queryAllOrders();
  res.send(orders);
};

const getOrdersByUser = async (req, res) => {
  logRequest("GET","/orders");
  const { userId } = req.query;
  const orders = await queryOrdersByUser(userId);
  res.send(orders);
};

const getOrder = async (req, res) => {
  const { orderId } = req.params;
  logRequest("GET","/order/"+orderId);
  const order = await queryOrderById(orderId);
  res.send(order);
};

const getUser = async (req, res) => {
  const { userId } = req.params;
  logRequest("GET","/user/"+userId);
  const user = await queryUserById(userId);
  res.send(user);
};

const getUsers = async (req, res) => {
  logRequest("GET","/users");
  const users = await queryAllUsers();
  res.send(users);
};

/* Implemented controller for POST orders */
const postOrder = async (req, res) => {
  logRequest("POST","/orders")
	const order = req.body

	process.stdout.write(`postOrder: ${JSON.stringify(order)}\n`)

	const response = await insertOrder(order)

	res.send(response)
}

const patchUser = async (req, res) => {
 	const updates = req.body;
 	const { userId } = req.params;
  logRequest("PATCH", "/user/"+userId);
	process.stdout.write(`patchUser: userId: ${userId}\n`)
	process.stdout.write(`patchUser: updates: ${JSON.stringify(updates)}\n`)

 	const response = await updateUser(userId, updates);

 	res.send(response);
};

module.exports = {
  init,
  getProduct,
  getRandomProduct,
  getCategories,
  getAllOrders,
  getOrdersByUser,
  getOrder,
  getProducts,
  getUser,
  getUsers,
  /* Export controller for POST orders */
  postOrder,
  patchUser,
};
