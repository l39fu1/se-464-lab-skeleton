#!/bin/bash

curl \
	--header 'Content-Type: application/json' \
	--request POST \
	--data '{ "user_id": "4023280f-c78c-4fec-b782-0b6cea1ed029", "products": [ { "product_id": "e937fa3b-8145-4c36-a0b9-c158895ce1ee", "quantity": 5 } ], "total_amount": 8 }' \
	localhost:3000/orders

# curl \
# 	--header 'Content-Type: application/json' \
# 	--request PATCH \
# 	--data '{ "id": "502358e5-a5fa-4b62-a194-e39eb740e383", "email": "john.jones@gmail.com", "name": "John Jones" }' \
# 	localhost:3000/user/502358e5-a5fa-4b62-a194-e39eb740e383
