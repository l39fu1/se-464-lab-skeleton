const db = process.argv[3];
const PROTO_PATH = "./app.proto";
const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");
const logger = require("./logger.js");
const {
  init,
  queryRandomProduct,
  queryUserById,
  queryAllProducts,
  queryProductById,
  queryAllCategories,
  queryAllOrders,
  queryOrdersByUser,
  queryOrderById,
  queryAllUsers,
  insertOrder,
  updateUser,
} = require(db === "dynamo"
  ? "./dynamo_db.js"
  : db === "sql"
  ? "./mysql_db.js"
  : "");
const { uuid } = require("uuidv4");

let requestCount = 0;

const logRequest = (requestPath) => {
  requestCount++;
  logger.info(
    `Request: rpc ${requestPath} received at ${new Date().toISOString()}`
  );
  logger.info(`Total number of requests: ${requestCount}`);

}

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});

const app = grpc.loadPackageDefinition(packageDefinition).App;
init();

async function randomProduct(call, callback) {
  logRequest("GetRandomProduct");
  const randProd = await queryRandomProduct();
  callback(null, randProd);
}

async function allProducts(call, callback) {
  logRequest("GetAllProducts");
  const products = await queryAllProducts();
  callback(null, { products });
}

async function product(call, callback) {
  const { product_id } = call.request;
  logRequest(`GetProduct(product_id=${product_id})`);
  const product = await queryProductById(product_id);
  console.log(product);
  callback(null, product);
}

async function categories(call, callback) {
  logRequest("GetAllCategories");
  const categories = await queryAllCategories();
  callback(null, { categories });
}

async function allOrders(call, callback) {
  logRequest("GetAllOrders");
  const orders = await queryAllOrders();
  callback(null, { orders });
}

async function ordersByUser(call, callback) {
  const { id } = call.request;
  logRequest(`GetAllOrdersByUser(id=${id})`);
  const orders = await queryOrdersByUser(id);
  callback(null, { orders });
}

async function order(call, callback) {
  const { id } = call.request;
  logRequest(`GetOrder(id=${id})`);
  const order = await queryOrderById(id);
  callback(null, order);
}

async function user(call, callback) {
  const { id } = call.request;
  logRequest(`GetUser(id=${id})`);
  const user = await queryUserById(id);
  callback(null, user);
}

async function users(call, callback) {
  logRequest("GetAllUsers");
  const users = await queryAllUsers();
  callback(null, { users });
}

//insert a new order and return the order
async function postOrder(call, callback) {
  const order = call.request;

  process.stdout.write(`GRPC: postOrder: order: ${JSON.stringify(order)}\n`)

  await insertOrder(order);

  callback(null, order);
}

async function accountDetails(call, callback) {
  const { id } = call.request;
  delete call.request.id;

  //console.log(`accountDetails: ${id}, ${JSON.stringify(call.request)}`)

  const user = await updateUser(id, call.request);
  callback(null, user);
}

/**
 * Starts an RPC server that receives requests for the Greeter service at the
 * sample server port
 */

const server = new grpc.Server();
server.addService(app.service, {
  getRandomProduct: randomProduct,
  getAllProducts: allProducts,
  getProduct: product,
  getAllCategories: categories,
  getAllOrders: allOrders,
  getAllUserOrders: ordersByUser,
  getOrder: order,
  getUser: user,
  getAllUsers: users,
  postOrder: postOrder,
  patchAccountDetails: accountDetails,
});

server.bindAsync(
  "0.0.0.0:3001",
  grpc.ServerCredentials.createInsecure(),
  () => {
    logger.info("App ready and listening on port 3001");
    server.start();
  },
);
