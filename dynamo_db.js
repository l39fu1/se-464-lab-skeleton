require("dotenv").config();
const logger = require("./logger");
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const {
  GetCommand,
  ScanCommand,
  PutCommand,
  UpdateCommand,
  DynamoDBDocumentClient,
} = require("@aws-sdk/lib-dynamodb");
const uuid = require("uuid");

let client;
let docClient;

const init = () => {
  client = new DynamoDBClient({ region: process.env.AWS_REGION });
  docClient = DynamoDBDocumentClient.from(client);
  logger.info("DynamoDB connected!");
};

const queryRandomProduct = async () => {
  /* Implemented */
  const maxRetry = 5

  /* Do up to maxRetry number of attemps */
  for (let i = 0; i < maxRetry; i++) {
    /* Generate random uuid key to start lookup from. This may be
        past the bounds of the dynamoDB hash keys table, so may fail */
    const id = uuid.v4()
    /* Scan for 1 item in position after the random row key */
    const command = new ScanCommand({
      TableName: 'Products',
      ExclusiveStartKey: { id: id },
      Limit: 1,
    })


    const response = await docClient.send(command)

    if (response.Items.length > 0) {
      return response.Items[0]
    }
  }

  /* If we failed every attempt, then we probably have very few items in our
      table. So we can scan everything and pick a random item from all
      table items */
  process.stdout.write('queryRandomProduct: miss\n')

  const command = new ScanCommand({
    TableName: 'Products',
  })

  const response = await docClient.send(command)

  const itemCount = response.Items.length

  if (itemCount === 0) {
    return null
  }

  const randomIndex = Math.floor(Math.random() * response.Items.length)
  return response.Items[randomIndex]
};

const queryProductById = async (productId) => {
  const command = new GetCommand({
    TableName: "Products",
    Key: {
      id: productId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllProducts = async (category = "") => {
  /* Implemented */
  const command = new ScanCommand({
    TableName: "Products",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllCategories = async () => {
  const command = new ScanCommand({
    TableName: "Categories",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllOrders = async () => {
  const command = new ScanCommand({
    TableName: "Orders",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrdersByUser = async (userId) => {
  const command = new ScanCommand({
    TableName: "Orders",
    FilterExpression: "user_id = :user_id",
    ExpressionAttributeValues: {
      ":user_id": userId,
    },
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrderById = async (userId) => {
  const command = new GetCommand({
    TableName: "Orders",
    Key: {
      id: userId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryUserById = async (userId) => {
  const command = new GetCommand({
    TableName: "Users",
    Key: {
      id: userId,
    },
    ProjectionExpression: "id, email",
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllUsers = async () => {
  const command = new ScanCommand({
    TableName: "Users",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const insertOrder = async (order) => {
  /* Implemented */
	const id = uuid.v4()
        console.log(id)

	const item = {
		...order,
		id: id,
	}

	/* This is a POST */
	const command = new PutCommand({
		TableName: 'Orders',
		Item: item,
	})

	const response = await docClient.send(command)

	process.stdout.write(`insertOrder: response: ${JSON.stringify(response)}\n`)

	return response
};

const updateUser = async (id, updates) => {
  /* Implemented */
  console.log(`updateUser: id: ${id}, updates: ${JSON.stringify(updates)}`)

	/* This is a PATCH */
	const command = new UpdateCommand({
		TableName: 'Users',
		Key: {
			id: id,
		},
		ExpressionAttributeNames: {
			'#email': 'email',
			'#password': 'password',
		},
		ExpressionAttributeValues: {
			':email': updates.email,
			':password': updates.password,
		},
		UpdateExpression: 'SET #email = :email, #password = :password',
		ReturnValues: 'ALL_NEW',
	})

	const response = await docClient.send(command)

	process.stdout.write(`${JSON.stringify(response)}\n`)

	return response
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
