require("dotenv").config();
const logger = require("./logger.js");
const util = require("util");
const uuid = require("uuid");
const mysql = require("mysql2-promise")();

const init = async () => {
  mysql.configure({
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT,
    database: process.env.RDS_DATABASE,
  });
   try { 
    mysql.query('SELECT * FROM users').spread(function (users) {
      logger.info("Database connected!");
    });
   } catch (e) {
      logger.info(e);
   }
};

// get a product by id
const queryProductById = async (productId) => {
  return (await mysql.query(`SELECT *
                              FROM products
                              WHERE id = "${productId}";`))[0][0];
};

//get random product: shuffle the products and return the first one
const queryRandomProduct = async () => {
  return (await mysql.query("SELECT * FROM products ORDER BY RAND() LIMIT 1;"))[0][0];
};

const queryAllProducts = async () => {
  return (await mysql.query("SELECT * FROM products;"))[0];
};

const queryAllCategories = async () => {
  return (await mysql.query("SELECT * FROM categories;"))[0];
};

const queryAllOrders = async () => {
  return (await mysql.query("SELECT * FROM orders;"))[0];
};


const queryOrdersByUser = async (userId) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                                    INNER JOIN order_items ON orders.id = order_items.order_id
                           WHERE user_id = "${userId}"`)
  )[0]; // Not a perfect analog for NoSQL, since SQL cannot return a list.
};

const queryOrderById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                           WHERE id = "${id}"`)
  )[0][0];
};

const queryUserById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM users
                           WHERE id = "${id}";`)
  )[0][0];
};

const queryAllUsers = async () => {
  return (await mysql.query("SELECT * FROM users"))[0];
};

//insert the order and each of the order items
const insertOrder = async (order) => {
  //create a new order id
  const order_id = uuid.v4();
  //insert the order
  const sql = 'INSERT INTO orders (id, user_id, total_amount) VALUES (?, ?, ?)';
  const values = [order_id, order.user_id, order.total_amount];
  const res = await mysql.query(sql, values);
  const sql2 = 'INSERT INTO order_items (id, order_id, product_id, quantity) VALUES (?, ?, ?, ?)';
  //insert each of the order items
  order.products.forEach(async (product) => {
    const values2 = [uuid.v4(), order_id, product.product_id, product.quantity];
    await mysql.query(sql2, values2);
  });
  
  return;
};

//update a user's info by id
const updateUser = async (id, updates) => {
  await mysql.query('UPDATE users SET ? WHERE id = ?', [updates, id]);
  return "ok";
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
